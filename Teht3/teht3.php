<!DOCTYPE html>
<html data-ng-app>
	<head>
		<meta charset="utf-8">
		<title>kotitehtävä 3 Kellosovellus</title>
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

		<script src="http://code.angularjs.org/1.2.10/angular.min.js"></script>
		<script src="http://code.angularjs.org/1.2.10/angular-animate.min.js"></script>

		<link rel="stylesheet" type="text/css" href="../animations.css">
	</head>

	<body>
		<h2>Kellosovellus</h2>
		<div data-ng-controller="clockCtrl">
				<h3>European times:</h3>

				<select data-ng-model="timezone">
					<?php foreach (timezone_abbreviations_list() as $zone) :
						foreach ($zone as $item) :
							if (strstr($item['timezone_id'], 'Europe') !== false) : ?>
								<option value="<?php echo $item['timezone_id']; ?>">
									<?php echo str_replace("Europe/", "", $item['timezone_id']); ?>
								</option>
					<?php endif; endforeach; endforeach; ?>
				</select>

				<h3>Time:</h3>

				<input type="text" value="" readonly="true" data-ng-model="time" data-ng-class="clock" data-ng-init="clock='size-s'" />
				<br />
				<button data-ng-click="clock='size-s'">Small</button>
				<button data-ng-click="clock='size-m'">Medium</button>
				<button data-ng-click="clock='size-l'">Large</button>

				<hr />

				<button data-ng-click="updateTime()">Päivitä päivä</button>
				<button data-ng-click="toggleTimer()">{{ clockToggle }}</button>
		</div>
	</body>
	<script>

		function clockCtrl($scope, $http, $interval) {
			
			function updateTime() {
				$http.get('server_time.php?timezone=' + $scope.timezone).success(function(currentTime) {
					$scope.time = currentTime;
				});
			};

			$scope.time = "";
			$scope.timezone = "Europe/Helsinki";
			$scope.updateTime = updateTime;

			var timer = $interval(updateTime, 1000);
			$scope.clockToggle = "Pysäytä kello";
			
			$scope.toggleTimer = function() {
				if (angular.isDefined(timer)) {
					$interval.cancel(timer);
					timer = undefined;
					$scope.clockToggle = "Jatka...";
				} else {
					timer = $interval(updateTime, 1000);
					$scope.clockToggle = "Pysäytä kello";
				}
			}
		};
	</script>
</html>