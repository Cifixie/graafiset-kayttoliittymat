<?php
	function isValidTimezoneId($tzid) {
		foreach (timezone_abbreviations_list() as $zone) {
			foreach ($zone as $item) {
				if ($item['timezone_id'] == $tzid) {
					return true;
				}
			}
		}
		return false;
	}

	function getParam($key) {
		return array_key_exists($key, $_GET) ? htmlspecialchars($_GET[$key], ENT_QUOTES) : null;
	}

	$requiredTimeZone = getParam('timezone');
	if (!empty($requiredTimeZone) && isValidTimezoneId($requiredTimeZone)) {
		$timeZone = new DateTimeZone($requiredTimeZone);
 		$date = new DateTime("now", $timeZone);
		echo $date->format('H:i:s');
	}
	else {
		echo "ERROR! Undifined timezone";
	}
?>