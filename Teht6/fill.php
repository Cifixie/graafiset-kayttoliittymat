<?php
namespace My;

//Ladataan boostrap.php (DatabaseManager -luokka)
require_once "bootstrap.php";

//Otetaan formin data nätimpään muuttujamuotoon
$formData = $_POST['form'];

//Luodaan uusi tietokantamanageri ja luodaan tietokanta, jos sitä ei ole
$databaseManager = new DatabaseManager();

//Luodaan formin datan perusteella tietokantaan taulu, jos sellaista ei ole.
if (!$databaseManager->hasTable($databaseManager::$TABLENAME)) {
    $databaseManager->createTable($databaseManager::$TABLENAME, array_keys($formData));
}

$formData['date'] = convertDateFormat('d.m.Y', $databaseManager::$DATEFORMAT, $formData['date']);

//Tallennetaan tiedot kantaan
$databaseManager->insert($databaseManager::$TABLENAME, $formData);

//jos tietokantaan lisäys onnistui, ohjataan sivusto takaisin etusivulle
if ($databaseManager->getLastInsertId()) {
    header('Location: index.php?success');
}
//Muussa tapauksessa annetaan virhe.
echo "Error! Can't insert data into database.";