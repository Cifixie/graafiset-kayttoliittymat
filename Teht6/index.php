<?php namespace My;
require_once "bootstrap.php"; ?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="lt-ie10" lang="en">
<![endif]-->
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GKL 1</title>

    <!-- Ladataan kaikki tarvittavat bowerkirjastojen css-tiedostot -->
    <link rel="stylesheet" href="bower_components/foundation/css/normalize.css"/>
    <link rel="stylesheet" href="bower_components/foundation/css/foundation.css"/>
    <link rel="stylesheet" href="bower_components/foundation/css/normalize.css"/>
    <link rel="stylesheet" href="bower_components/jquery.ui/themes/base/jquery.ui.all.css"/>
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"/>

    <!-- Ladataan modernizr-kirjasto, jos selaimena vanha IE vesio -->
    <script src="bower_components/foundation/js/vendor/modernizr.js"></script>

    <!-- Ladataan sivuston vaatimat js-kirjastot bowerin kautta -->
    <script src="bower_components/jquery/jquery.js"></script>
    <script src="bower_components/jquery.ui/ui/jquery.ui.core.js"></script>
    <script src="bower_components/jquery.ui/ui/jquery.ui.datepicker.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>

    <!-- Listään oma kustomointiin vaadittava oma css-tiedosto -->
    <link rel="stylesheet" href="styles.css"/>
</head>

<body>
<!-- Näytetään käyttäjälle palautetta, jos tapahtuman lisäys onnistui -->
<?php if (array_key_exists('success', $_GET)) : ?>
    <div class="row top-margin">
        <div data-alert class="alert-box success radius small-12 columns">
            Tapahtuma tallennettu.
            <a href="#" class="close">&times;</a>
        </div>
    </div>
<?php endif; ?>

<!-- Sivuston otsikko -->
<header class="row">
    <h1 class="large-12 text-center columns">yksinkertainen kalenterisovellus</h1>
</header>

<!-- Tabejen otsikkot omalla rivillä (Zurb foundation tabs) -->
<div class="row">
    <div class="large-12 columns">
        <dl class="tabs" data-tab>
            <dd class="active"><a href="#add-new-event">Lisää uusi tapahtuma</a></dd>
            <dd><a href="#list-all-events">Listaa kaikki tapahtumat</a></dd>
        </dl>
    </div>
</div>

<!-- Tästä alkaa tabien sisältö -->
<div class="tabs-content">
    <div class="content active" id="add-new-event">

        <!-- Formi tapahtuman lisäystä varten, pohjana Zurb Foundation form -->
        <form data-abide action="fill.php" method="post" novalidate="novalidate">
            <div class="row">
                <div class="name-field medium-7 columns">
                    <label>Tapahtuman nimi
                        <small>required</small>
                        <input name="form[name]" type="text" required/>
                    </label>
                    <small class="error">Nimitieto on pakollinen</small>
                </div>
                <div class="date-field medium-5 columns">
                    <label>Päivämäärä
                        <small>required</small>

                        <!-- Annetaan parametri readonly, jolloin vain datepicker voi asetta kentälle arvon. Validoidaan se silti regex patternilla -->
                        <input id="datepicker" name="form[date]" type="text" required readonly
                               pattern="^[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}$">
                    </label>
                    <small class="error">Päivämäärä on pakollinen tieto!</small>
                </div>
            </div>
            <div class="row">
                <div class="description-field medium-12 columns">
                    <label>Tapahtuman kuvaus
                        <small>required</small>
                        <input name="form[description]" type="text" required>
                    </label>
                    <small class="error">Nimitieto on pakollinen</small>
                </div>
            </div>
            <div class="row">
                <div class="email-field medium-6 columns">
                    <label>Sähköpostiosoite
                        <small>required</small>
                        <input name="form[email]" type="email" required>
                    </label>
                    <small class="error">Sähköposti tulee olla kelvollinen</small>
                </div>
                <div class="phone-field medium-6 columns">
                    <label>Puhelinnumero
                        <small>required</small>

                        <!-- koska en itse löytänyt hyvää regexiä puhelinnumerolle, kehitin oman, mikä on vähän kehno, mutta hoitaa perusteet -->
                        <input name="form[phone]" type="text" pattern="^[+358|0][0-9 ]{9,13}" required>
                    </label>
                    <small class="error">Puhelinnumeron tulee olla kelvollinen</small>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <button type="clear" class="button radius reddish">Tyhjennä</button>
                    <button type="submit" class="button radius greenish">Lähetä</button>
                </div>
            </div>
        </form>
    </div>
    <div class="content" id="list-all-events">
        <div class="row">
            <div class="large-12 columns">

                <!-- Luodaan databaseManager (bootstrap.php) tietokanta käsittelyjä varten -->
                <?php $databaseManager = new DatabaseManager();

                    //Jos tietokanta on olemassa (oletusti pitää sisällään väh. yhden tapahtuman), listataan kaikki tapahtumat käyttäjälle
                    if ($databaseManager->hasTable($databaseManager::$TABLENAME)) : ?>

                        <!-- Listauksessa käytetään Zurb Foundation accordion:ia -->
                        <dl class="accordion large-12 columns" data-accordion>
                            <?php
                                //Luodaan queryBuilder tietokantakyselyä varten
                                $queryBuilder = $databaseManager->createQueryBuilder();
                                //Haetaan kaikki tiedot 'TEHT6'-taulusta (bootstrap.php) järjestettynä päivämäärän mukaan.
                                $queryBuilder->select('*')->from($databaseManager::$TABLENAME, '')->orderBy('date');
                                //Käydään kaikki vastaukset läpi:
                                foreach ($queryBuilder->execute() as $result) : ?>
                                    <dd>
                                        <!-- Muodostetaan otsikko, jossa hyödynnetään ID-tietoa -->
                                        <a href="#event-<?php echo $result['id']; ?>">
                                            <!-- otsikko on muodossa: 1.12.2012 - esimerkki -->
                                            <?php echo convertDateFormat($databaseManager::$DATEFORMAT, 'd.m.Y', $result['date']) . ' - ' . htmlspecialchars($result['name'], ENT_HTML5); ?>
                                        </a>
                                        <div id="event-<?php echo $result['id']; ?>" class="content">
                                            <!-- tulostetaan loput tiedot -->
                                            <p class="lead"><?php echo htmlspecialchars($result['description'], ENT_HTML5); ?></p>
                                            <!-- puhelin numeron ja sähköpostin idikaattoreina käytetään font-awesomen kuvakkeita -->
                                            <p><i class="fa fa-envelope"></i> : <?php echo htmlspecialchars($result['email'], ENT_HTML5); ?></p>
                                            <p><i class="fa fa-phone"></i> : <?php echo htmlspecialchars($result['phone'], ENT_HTML5); ?></p>
                                        </div>
                                    </dd>
                                <!-- Päätetään looppaus -->
                                <?php endforeach; ?>
                        </dl>
                <?php else: ?>
                        <!-- Jos tietokantaa ei löydy, annetaan siitä looginen virheviesti käyttäjälle -->
                        <p class="lead">Ei tapahtumia</p>
                        <p>Sinun tulee ensin luoda vähintään yksi tapahtuma.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    // Kun sivusto on valmis: (jquery document on ready -statement)
    $(function () {
        //käynnistetään foundation
        $(document).foundation();

        //Määritellään datepicker
        $("#datepicker").datepicker({
            dateFormat: "dd.mm.yy"
        });
    });
</script>
</html>