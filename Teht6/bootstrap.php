<?php
namespace My;

// Ladataan composer:in autoloader.
require_once "vendor/autoload.php";

// määritellään käytetyt luokat
use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Schema\Schema;
use \DateTime;

function convertDateFormat($from, $to, $date) {
    $dateTime = DateTime::createFromFormat($from, $date);
    return $dateTime->format($to);
}

class DatabaseManager {

    //määritellään staattinen muuttuja, taulukon nimi. mitä tarvitaan myöhemmin.
    static $TABLENAME = 'TEHT6';
    static $DATEFORMAT = 'Y-m-d';

    //määritellään yksityiset muuttujat, doctrine komponentteja.
    private $databaseConnection;
    private $schemaManager;
    private $databasePlatform;

    public function __construct() {
        $config = new Configuration();

        //Asetetaan Doctrinen DriverManagerin konffit käyttämänä sqliteä ja annetaan polku tietokannalle.
        $connectionParameters = array(
            'dbname' => 'tommikes',
            'host' => 'mysql.metropolia.fi',
            'driver' => 'pdo_mysql',
            'user' => 'tommikes',
            'password' => 'z1J5ER8NrJRYB3ZyhZ'
        );

        // Luodaan vaadittavat doctrine komponentit
        $this->databaseConnection = DriverManager::getConnection($connectionParameters, $config);
        $this->schemaManager = $this->databaseConnection->getSchemaManager();
        $this->databasePlatform = $this->databaseConnection->getDatabasePlatform();
    }

    //Metodi palauttaa booleanin, onko taulua luotu.
    public function hasTable($tableName) {
        return in_array($tableName, $this->schemaManager->listTableNames());
    }

    //Tallenta tiedot kantaan
    public function insert($tableName, array $data) {
        return $this->databaseConnection->insert($tableName, $data);
    }

    //Hakee viimeksi talletun tiedon ID:n
    public function getLastInsertId() {
        return $this->databaseConnection->lastInsertId();
    }

    //Luo queryBuilderin
    public function createQueryBuilder() {
        return $this->databaseConnection->createQueryBuilder();
    }

    //Tekee tietokantaan taulun, ja lisää sinne sarakkeita sen mukaa, mitä array pitää sisällään.
    //Oletuksena kaikki String-tyyppisiä, paitsi date on tietenkin date.
    //Lisää myös jokaiseen tauluun oletuksena ID:n joka on autoincrement ja primary key
    public function createTable($tableName, array $columNames) {
        $schema = new Schema();
        //Luodaan uusi taulu
        $table = $schema->createTable($tableName);

        //Määritellään 1 kolumniksi ID, joka on autoingrement ja taulun primary key
        $table->addColumn('id','integer')->setAutoincrement(true);
        $table->setPrimaryKey(array('id'));

        //loopataan sarakelista läpi
        foreach ($columNames as $columName) {
            //määritellään sarakkeen tyyppi nimen perusteella: jos nimi on date, luodaan päivämäärä-tyyppi, muutoin merkkijono
            $type = ($columName == 'date') ? 'date' : 'string';
            $table->addColumn($columName, $type);
        };

        //suoritetaan kaikki vaadittavat kyselyt.
        $this->executeQueries(
            $schema->toSql($this->databasePlatform)
        );
    }

    //Suorittaa kaikki listatut kyselyt.
    private function executeQueries(array $queries) {
        foreach ($queries as $query) {
            $this->databaseConnection->exec($query);
        }
    }
}